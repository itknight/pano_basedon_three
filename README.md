#pano_basedon_three
基于Three.js的360X180度全景图预览插件

预览效果图1 -进入界面

![预览效果图1- 进入界面](http://git.oschina.net/itknight/pano_basedon_three/raw/master/preview/preview_1.png?dir=0&filepath=preview%2Fpreview_1.png&oid=94121e07ac197fa944c2324182c3ee0a4add4d10&sha=dcbcf158725b43c7719d8be0f34a47f6204e2fe0)

预览效果图2 -预置在内部的一个全景图效果


![预览效果图2 - 预置在内部的一个全景图效果](http://git.oschina.net/itknight/pano_basedon_three/raw/master/preview/preview_2.png?dir=0&filepath=preview%2Fpreview_2.png&oid=9b0f9eecbb54fb85b6c1c2c9d85350e6e45a8366&sha=dcbcf158725b43c7719d8be0f34a47f6204e2fe0)